class AccessToken < ApplicationRecord
  belongs_to :student, optional: true

  validates :token, presence: true
end
