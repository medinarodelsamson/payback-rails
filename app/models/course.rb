class Course < ApplicationRecord
  has_and_belongs_to_many :professors
  has_many :ratings, dependent: :destroy

  validates :name, presence: true
end
