class Rating < ApplicationRecord
  belongs_to :student, optional: true
  belongs_to :professor, optional: true
  belongs_to :course, optional: true

  validates :score, presence: true, numericality: { only_integer: true }, inclusion: { in: 1..5, message: 'should within 1 to 5.'}
end
