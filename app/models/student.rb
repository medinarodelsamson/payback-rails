class Student < ApplicationRecord
  include Authenticatable
  include TokenProcessor

  has_many :access_tokens
  has_many :ratings

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates_email_format_of :email

  def self.find_by_credentials(credentials)
    user = self.find_by(email: credentials.fetch(:email, ''))
    user if user.present? && user.valid_password?(credentials.fetch(:password, ''))
  end
end