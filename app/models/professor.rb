class Professor < ApplicationRecord
  has_and_belongs_to_many :courses
  has_many :ratings, dependent: :destroy

  validates :name, presence: true
end
