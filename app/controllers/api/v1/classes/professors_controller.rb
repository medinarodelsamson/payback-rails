class Api::V1::Classes::ProfessorsController < ApiController
  def index
    render_obj({
          professors: course.professors.order(:name),
          ratings: course.ratings
    })
  end

  private
  def course
    @course ||= Course.find params[:class_id]
  end
end