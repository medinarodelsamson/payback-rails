class Api::V1::Classes::Professors::RateController < ApiController
  def create
    obj = current_user.ratings.where(course_id: course, professor: professor).first_or_initialize
    obj.score = obj_params[:score]
    obj.comment = obj_params[:comment]

    if obj.save
      render_obj obj
    else
      obj_errors obj
    end
  end

  private
  def course
    @course ||= Course.find params[:class_id]
  end

  def professor
    @professor ||= Professor.find params[:professor_id]
  end

  private
  def obj_params
    params.require(:rating).permit(:score, :comment)
  end
end