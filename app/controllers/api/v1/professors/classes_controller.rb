class Api::V1::Professors::ClassesController < ApiController
  def index
    render_obj({
          classes: professor.courses.order(:name),
          ratings: professor.ratings.order(created_at: :desc)
    })
  end

  private
  def professor
    @professor ||= Professor.find params[:professor_id]
  end
end