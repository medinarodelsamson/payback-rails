class Api::V1::ProfessorsController < ApiController
  def show
    render_obj Professor.find params[:id]
  end
end