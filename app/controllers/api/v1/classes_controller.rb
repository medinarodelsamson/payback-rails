class Api::V1::ClassesController < ApiController
  def index
    render_collection Course.order(:name)
  end

  def show
    render_obj Course.find params[:id]
  end
end