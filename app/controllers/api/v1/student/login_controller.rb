class Api::V1::Student::LoginController < ApiController
  skip_before_action :authenticate_request, only: :create

  def create
    obj = ::Student.find_by_credentials(params[:user])
    if obj.present? && obj.set_access_token
      render_obj(Users::Builder.new(params).user_attr(obj))
    else
      render_error("Your email and password don't match", 401)
    end
  end
end