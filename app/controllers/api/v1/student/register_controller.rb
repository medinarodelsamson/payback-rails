class Api::V1::Student::RegisterController < ApiController
  skip_before_action :authenticate_request, only: :create

  def create
    obj = ::Student.create(obj_params)
    if obj.valid? && obj.set_access_token
      render_obj(Users::Builder.new(params).user_attr(obj))
    else
      obj_errors obj
    end
  end

  private
  def obj_params
    params.require(:user).permit(:name, :email, :password)
  end
end