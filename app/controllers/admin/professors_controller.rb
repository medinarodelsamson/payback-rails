class Admin::ProfessorsController < AdminController
  before_action :obj, only: [:edit, :update, :destroy]
  def index
    @objs = klass.order(:name)
  end

  def new
    @obj = klass.new
  end

  def create
    @obj = klass.new permitted_params

    if @obj.save
      redirect_to admin_professors_path, notice: 'Created!'
    else
      render :new
    end
  end

  def edit
    
  end

  def update
    if @obj.update(permitted_params)
      redirect_to admin_professors_path, notice: 'Updated!'
    else
      render :edit
    end
  end

  def destroy
    if @obj.destroy
      redirect_to admin_professors_path, notice: 'Deleted!'
    else
      redirect_to admin_professors_path, alert: 'Unable to delete!'
    end
  end

  private
  def permitted_params
    params.require(klass.to_s.downcase.to_sym).permit( 
      :name, 
      course_ids: []
    )
  end

  def klass
    Professor
  end

  def obj
    @obj = klass.find params[:id]
  end
end