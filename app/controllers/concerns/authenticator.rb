module Authenticator
  extend ActiveSupport::Concern

  def current_user
    @current_user ||= Student.find_by_valid_token request_details, 'AccessToken'
  end

  def request_details
    @request_details ||=
      {
        access_token: request.headers['AccessToken']
      }
  end
end
