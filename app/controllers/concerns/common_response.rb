module CommonResponse
  extend ActiveSupport::Concern

  def render_collection collection=nil
    render json: {collection: (collection || @collection)}
  end

  def render_obj obj = nil
    render json: obj
  end

  def render_success
    render json: {success: true}
  end

  def obj_errors obj=nil
    render json: { message: 'Validation failed', errors: [obj.errors.messages] }, status: 422
  end

  def render_error(error, status = 422)
    render json: { errors: [error] }, status: status
  end
end
