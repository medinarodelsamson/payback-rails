class ApiController < ActionController::Base
  include CommonResponse
  include Authenticator

  protect_from_forgery with: :null_session
  before_action :authenticate_request

  rescue_from ActiveRecord::RecordNotFound, with: :not_found


  def authenticate_request
    return render_error('Unauthorized.', 401) unless current_user.present?
  end

  def not_found
    render_error('Not found.')
  end
end