module Users
  class Builder
    def initialize params = nil
      @params = params
    end

    def user_attr obj
      {
        id: obj.id,
        name: obj.name,
        email: obj.email,
        access_token: obj.current_token,
      }
    end
  end
end
