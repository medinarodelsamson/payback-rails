# PaybackTime

## Setup
```console 
bundle install
cp config/application.yml.example config/application.yml (then fill up the required keys)
```

```console 
cp config/database.yml.example config/database.yml
rails db:create
rails db:migrate
rails db:seed
```

Optional
```console 
rails dev:seed
```

#### Admin
http://localhost:3000/admin

### Testing.
```console 
rspec
```