class CreateRatings < ActiveRecord::Migration[5.2]
  def change
    create_table :ratings do |t|
      t.belongs_to :student
      t.belongs_to :course
      t.belongs_to :professor
      t.integer :score, default: 0
      t.text :comment

      t.timestamps
    end
  end
end
