namespace :admin do
  root 'courses#index'

  resources :courses
  resources :professors
end # namespace :api do