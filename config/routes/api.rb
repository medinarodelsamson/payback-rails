namespace :api do
  namespace :v1 do
    namespace :student do
      resources :login, only: [:create]
      resources :register, only: [:create]
    end

    resources :classes, only: [:index, :show] do
      resources :professors, only: [:index], controller: 'classes/professors' do
        resources :rate, only: [:create], controller: 'classes/professors/rate'
      end
    end

    resources :professors, only: [:index, :show] do
      resources :classes, only: [:index], controller: 'professors/classes'
    end
  end # namespace :v1 do
end # namespace :api do