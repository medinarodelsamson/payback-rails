require 'rails_helper'

RSpec.describe Api::V1::Classes::ProfessorsController, type: :request do
  # list of professors & ratings

  let!(:user) { DataSeed.new_student }
  let!(:access_token) { user.access_tokens.first.token }

  it 'requires valid access_token' do
    api_get_request '/api/v1/classes/1/professors'

    expect(response).to have_http_status(401)
  end

  it 'returns collection of professors & ratings' do
    obj = DataSeed.new_course(professor_ids: [DataSeed.new_professor.id])

    api_get_request ['/api/v1/classes/', obj.id, '/professors'].join, {}, {'AccessToken' => access_token}
    body = JSON.parse response.body

    expect(body['professors'].count).to eq(1)
    expect(body['ratings'].count).to eq(0)
    expect(response).to have_http_status(200)
  end
end