require 'rails_helper'

RSpec.describe Api::V1::Classes::Professors::RateController, type: :request do
  # create Rating

  let!(:user) { DataSeed.new_student }
  let!(:access_token) { user.access_tokens.first.token }

  it 'requires valid access_token' do
    api_post_request '/api/v1/classes/1/professors/1/rate'

    expect(response).to have_http_status(401)
  end

  it 'creates new Rating' do
    prof = DataSeed.new_professor
    obj  = DataSeed.new_course(professor_ids: [prof.id])
    params = {rating: {score: 5, comment: 'foo'}}

    expect{
      api_post_request ['/api/v1/classes/', obj.id, '/professors/', prof.id, '/rate'].join, params, {'AccessToken' => access_token}
    }.to change{Rating.count}.from(0).to(1)
  end
end