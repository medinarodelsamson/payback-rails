require 'rails_helper'

RSpec.describe Api::V1::ClassesController, type: :request do
  # list of courses

  let!(:user) { DataSeed.new_student }
  let!(:access_token) { user.access_tokens.first.token }

  it 'requires valid access_token' do
    api_get_request '/api/v1/classes'

    expect(response).to have_http_status(401)
  end

  it 'returns collection' do
    DataSeed.new_course
    api_get_request '/api/v1/classes', {}, {'AccessToken' => access_token}

    body = JSON.parse response.body
    expect(body['collection'].count).to eq(1)
    expect(response).to have_http_status(200)
  end
end