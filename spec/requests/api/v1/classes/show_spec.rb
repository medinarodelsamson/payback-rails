require 'rails_helper'

RSpec.describe Api::V1::ClassesController, type: :request do
  # course detail

  let!(:user) { DataSeed.new_student }
  let!(:access_token) { user.access_tokens.first.token }

  it 'requires valid access_token' do
    api_get_request '/api/v1/classes/1'

    expect(response).to have_http_status(401)
  end

  it 'return Course object' do
    obj = DataSeed.new_course
    api_get_request ['/api/v1/classes/', obj.id].join, {}, {'AccessToken' => access_token}

    body = JSON.parse response.body
    expect(body['name']).to eq(obj.name)
    expect(response).to have_http_status(200)
  end
end