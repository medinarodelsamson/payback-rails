require 'rails_helper'

RSpec.describe Api::V1::Professors::ClassesController, type: :request do
  # list of classes & ratings

  let!(:user) { DataSeed.new_student }
  let!(:access_token) { user.access_tokens.first.token }

  it 'requires valid access_token' do
    api_get_request '/api/v1/professors/1/classes'

    expect(response).to have_http_status(401)
  end

  it 'returns collection of classes & ratings' do
    obj = DataSeed.new_professor(course_ids: [DataSeed.new_course.id])

    api_get_request ['/api/v1/professors/', obj.id, '/classes'].join, {}, {'AccessToken' => access_token}
    body = JSON.parse response.body

    expect(body['classes'].count).to eq(1)
    expect(body['ratings'].count).to eq(0)
    expect(response).to have_http_status(200)
  end
end