require 'rails_helper'

RSpec.describe Api::V1::ProfessorsController, type: :request do
  # professor detail

  let!(:user) { DataSeed.new_student }
  let!(:access_token) { user.access_tokens.first.token }

  it 'requires valid access_token' do
    api_get_request '/api/v1/professors/1'

    expect(response).to have_http_status(401)
  end

  it 'return Professor object' do
    obj = DataSeed.new_professor
    api_get_request ['/api/v1/professors/', obj.id].join, {}, {'AccessToken' => access_token}

    body = JSON.parse response.body
    expect(body['name']).to eq(obj.name)
    expect(response).to have_http_status(200)
  end
end