require 'rails_helper'

RSpec.describe Api::V1::Student::LoginController, type: :request do
  # allows you to login user

  # vanilla login
  context 'vanilla login' do
    let!(:user) { 
      Student.create(
          name: 'foo',
          email: 'foo@bar.com',
          password: 'password'
      )
    }

    it 'returns user info if successful login' do
      params = { user: {
          email: user.email,
          password: 'password'
        }
      }

      api_post_request '/api/v1/student/login', params, {}

      expect(response).to have_http_status(200)

      body = JSON.parse response.body
      expect(body['access_token'].present?).to be_truthy
    end

    it 'fails to login if incorrect info' do
      params = { user: {
          email: user.email,
        }
      }

      api_post_request '/api/v1/student/login', params, {}

      expect(response).to have_http_status(401)

      body = JSON.parse response.body
      expect(body['errors'].present?).to be_truthy
    end
  end
end