require 'rails_helper'

RSpec.describe Api::V1::Student::RegisterController, type: :request do
  # allows you to register user

  # vanilla registration
  context 'vanilla registration' do
    it 'registers user' do
      params = { user: {
          name: 'Foo',
          email: 'foo@bar.com',
          password: 'password'
        }
      }

      api_post_request '/api/v1/student/register', params, {}

      expect(response).to have_http_status(200)

      body = JSON.parse response.body
      expect(body['access_token'].present?).to be_truthy
    end

    it 'fails to register user if some required missing ' do
      params = { user: {
          name: 'Foo',
          email: '',
          password: ''
        }
      }

      api_post_request '/api/v1/student/register', params, {}

      expect(response).to have_http_status(422)

      body = JSON.parse response.body
      expect(body['errors'].present?).to be_truthy
    end
  end
end