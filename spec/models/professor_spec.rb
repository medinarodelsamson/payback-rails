require 'rails_helper'

RSpec.describe Professor, type: :model do
  it { should have_and_belong_to_many(:courses) }
  it { should have_many(:ratings) }

  it { should validate_presence_of(:name) }
end
