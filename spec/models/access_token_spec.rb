require 'rails_helper'

RSpec.describe AccessToken, type: :model do
  it { should belong_to(:student) }

  it { should validate_presence_of(:token) }
end
