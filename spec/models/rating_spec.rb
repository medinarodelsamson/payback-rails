require 'rails_helper'

RSpec.describe Rating, type: :model do
  it { should belong_to(:student) }
  it { should belong_to(:professor) }
  it { should belong_to(:course) }

  it { should validate_presence_of(:score) }
  it { should validate_numericality_of(:score).only_integer }
  it { should validate_inclusion_of(:score).in_array((1..5).to_a) }
end
