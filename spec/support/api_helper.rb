module ApiHelper
  def api_get_request(path, params={}, headers={})
    headers.merge!('Accept' => 'application/json')
    get path, params: params, headers: headers
  end

  def api_post_request(path, params={}, headers={})
    headers.merge!('Accept' => 'application/json')
    post path, params: params, headers: headers
  end

  def api_put_request(path, params={}, headers={})
    headers.merge!('Accept' => 'application/json')
    put path, params: params, headers: headers
  end

  def api_delete_request(path, params={}, headers={})
    headers.merge!('Accept' => 'application/json')
    delete path, params: params, headers: headers
  end
end