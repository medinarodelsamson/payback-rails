module DataSeed
  module_function

  def new_student options={}
    obj = Student.create(
      name: options[:name] || 'Foo',
      email: options[:email] || 'foo@bar.com',
      password: options[:password] || '12345678'
    )
    obj.set_access_token
    obj
  end

  def new_professor options={}
    obj = Professor.create(
      name: options[:name] || 'Foo',
      course_ids: options[:course_ids] || [],
    )
    obj
  end

  def new_course options={}
    obj = Course.create(
      name: options[:name] || 'Foo',
      professor_ids: options[:professor_ids] || [],
    )
    obj
  end
end