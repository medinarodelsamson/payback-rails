namespace :dev do
  desc "DEV related stuffs like setup"
  task seed: :environment do

    puts "seeding students..."
    50.times do |i|
      Student.create(name: Faker::FunnyName.name, email: "student#{i+1}@example.com", password: "password")
    end
    puts "done"

    puts "seeding courses..."
    5.times do
      Course.create(name: Faker::ProgrammingLanguage.name)
    end
    puts "done"

    puts "seeding professors..."
    5.times do
      Professor.create(name: Faker::Name.prefix + " " + Faker::Name.name)
    end
    puts "done"

    puts "seeding professor courses..."
    course_ids = Course.pluck(:id)
    Professor.all.each do |obj|
      obj.course_ids = course_ids.shuffle.take( (2..4).to_a.sample )
    end
    puts "done"

    puts "seeding rating..."
    courses = Course.all
    Student.all.each do |obj|
      courses.shuffle.take( (2..3).to_a.sample ).each do |course|
        obj.ratings.create(
          course_id: course.id, 
          professor_id: course.professors.order('random()').first.id,
          score: (1..5).to_a.sample,
          comment: Faker::ChuckNorris.fact
        )
      end
    end
    puts "done"

    puts "nothing to follow"
  end
end